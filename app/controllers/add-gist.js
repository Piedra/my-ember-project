import Controller from "@ember/controller";
import { action } from "@ember/object";
import { inject as service } from "@ember/service";

export default class AddGistController extends Controller {
  @service gistsInfo;

  @action
  addGistInfoRoute(body, description, fileName) {
    if (body == null || description == null || fileName == null) {
      alert("You must complete all the fields");
    } else {
      this.gistsInfo.addGist({ body, description, fileName });
      this.transitionToRoute("/");
    }
  }
}
