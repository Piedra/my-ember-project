import Service from "@ember/service";
import { tracked } from "@glimmer/tracking";

export default class GistsInfoService extends Service {
  @tracked gists = [
    {
      fileName: "Clean The House",
      description: "With the Broom",
      body: "Clean the house with the broom, and vacum please."
    }, {
      fileName: "Wash the Car",
      description: "Wash the Hyundai Accent",
      body: "Please wash the Hyundai Accent color blue, because a customer wants to see it"
    }, {
      fileName: "Make Lunch",
      description: "Family is coming, make lunch",
      body: "Grab the chicken out of the fridge and prepare it for the meal on the night"
    }, {
      fileName: "Make bobby take a bath",
      description: "Make your little brother to take a shower",
      body: "Make your brother bobby take a bath because he hasn't showered on 3 days"
    }
  ]

  addGist(gist) {
    this.gists.addObject(gist);
  }
}
